package main

import (
	"log"
	"net"
	"net/http"
	pbUser "user/proto"
	"user/service"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"google.golang.org/grpc"
)

func main() {
	conf, err := service.SetupVars()
	if err != nil {
		log.Fatal(err)
	}

	us, err := service.New(conf)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		log.Fatal(us.Stop())
	}()

	// health
	go func() {
		http.HandleFunc("/healthz", health)
		log.Printf("serving healthz on port %s", conf.HealthAddr)
		log.Fatal(http.ListenAndServe(conf.HealthAddr, nil))
	}()

	// prometheus
	go func() {
		http.Handle("/metrics", promhttp.Handler())
		log.Printf("serving prometheus on port %s", conf.PrometheusAddr)
		log.Fatal(http.ListenAndServe(conf.PrometheusAddr, nil))
	}()

	srv := grpc.NewServer(grpc.UnaryInterceptor(service.RecoverInterceptor))
	pbUser.RegisterUserServiceServer(srv, us)

	listener, err := net.Listen("tcp", conf.ServiceAddr)
	if err != nil {
		log.Fatalf("user service failed to listen: %v", err)
	}

	log.Printf("starting user service on %s", conf.ServiceAddr)
	log.Fatal(srv.Serve(listener))
}

func health(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}
