create table users (
    id serial primary key,
    email varchar(256) unique not null default '',
    pwd_hash varchar(64) not null default '',
    role tinyint not null default 0,
    status tinyint not null default 0,
    display_name varchar(50) unique not null default '',
    phone_number varchar(15) not null default '',
    birth_date bigint not null default 0,
    created_at int null,
    updated_at int null
)engine=InnoDB default charset=utf8 collate=utf8_general_ci;

create trigger ts_int_update before update on users
for each row begin
    set new.updated_at = unix_timestamp(now());
end;

create trigger ts_int_insert before insert on users
for each row begin
    if new.created_at is null and new.updated_at is null
    then 
        set new.created_at = unix_timestamp(now());
        set new.updated_at = unix_timestamp(now());
    end if;
end;