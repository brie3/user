insert into users (email, pwd_hash, role, status, display_name, phone_number, birth_date) values
(
    "bob@mail.ru",
    "09d7e08fa2a2194638215c65ea9c005aba25f70fb76e8a1b7607b723b26e3348",
    1,
    1,
    "Bob",
    "5417543010",
    348171375
), (
    "alice@mail.ru",
    "4a2db4d2a7c32acc31f6cd5fc82b93afa985dc16019cf65b0c39dc980a6ed9f1",
    2,
    1,
    "Alice",
    "5417341010",
    38686575
);