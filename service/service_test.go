package service

import (
	"context"
	"database/sql"
	"fmt"
	"math/rand"
	"testing"
	"time"
	pbUser "user/proto"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database"
	"github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
)

const (
	rusLength = 409600
	engLength = 676000
)

func randomRussian(rng *rand.Rand, length int) []string {
	n := rng.Perm(rusLength)
	out := make([]string, length)
	for i := 0; i < length; i++ {
		out[i] = fmt.Sprintf("%c%c%c%c", 'А'+n[i]/6400, 'А'+n[i]%6400/1000, 'А'+n[i]%640/10, 'А'+n[i]%64)
	}
	return out
}

func randomEnglish(rng *rand.Rand, length int) []string {
	n := rng.Perm(engLength)
	out := make([]string, length)
	var a int
	for i := 0; i < length; i++ {
		if randBool(rng) {
			a = 'A'
		} else {
			a = 'a'
		}
		out[i] = fmt.Sprintf("%c%c%c%c%c", a+n[i]/26000, a+n[i]%26000/1000, a+n[i]%2600/100, a+n[i]%260/10, a+n[i]%26)
	}
	return out
}

func randPhone(rng *rand.Rand, length int) string {
	var r rune
	n := rng.Intn(length)
	runes := make([]rune, n)
	for i := 0; i < n; i++ {
		r = rune(rng.Intn(10) + '0')
		runes[i] = r
	}
	return string(runes)
}

func randBool(rng *rand.Rand) bool {
	return rng.Intn(2) == 1
}

func TestService(t *testing.T) {
	conf := &Config{
		MaxOpenConns:   3,
		MaxIdleConns:   3,
		ConnectRetries: 3,
		SleepSeconds:   3,
		DBHost:         "10.0.2.15", // TODO exclude explicit ip setup
		DBName:         "grpc",
		DBUser:         "db_user",
		DBPwd:          "db_pwd",
	}
	var (
		drv       database.Driver
		migration *migrate.Migrate
		pool      *sql.DB
		err       error
	)
	if pool, err = dbConnect(conf); err != nil {
		t.Error(err)
		return
	}

	defer func() {
		pool.Close()
	}()

	drv, err = mysql.WithInstance(pool, &mysql.Config{DatabaseName: conf.DBName})
	if err != nil {
		t.Error(err)
		return
	}

	migration, err = migrate.NewWithDatabaseInstance("file://../migrations", conf.DBName, drv)
	if err != nil {
		t.Error(err)
		return
	}

	defer func() {
		if err := migration.Down(); err != nil {
			t.Error(err)
		}
	}()

	if err = migration.Steps(1); err != nil && err != migrate.ErrNoChange {
		t.Error(err)
		return
	}
	pool.SetConnMaxLifetime(conf.ConnMaxLifetime)
	pool.SetMaxOpenConns(conf.MaxOpenConns)
	pool.SetMaxIdleConns(conf.MaxIdleConns)
	serv := &UserService{db: pool}

	rng := rand.New(rand.NewSource(time.Now().UTC().UnixNano()))
	ctx := context.Background()

	eng := randomEnglish(rng, 200)
	rus := randomRussian(rng, 200)

	// CreateUser tests
	createUserTest := func() {
		var (
			cur          *pbUser.CreateUserRequest
			role, status int32
			date         int64
		)
		for i := int32(1); i < 100; i++ {
			phone := randPhone(rng, 10)
			role = i % 2
			status = i % 2
			date = time.Date(int(i), time.Month(i%12), 1, 0, 0, 0, 0, time.UTC).Unix()

			cur = &pbUser.CreateUserRequest{
				Email:       eng[i],
				PwdHash:     eng[i],
				Role:        pbUser.UserRole(role),
				Status:      pbUser.UserStatus(status),
				DispayName:  rus[i],
				PhoneNumber: phone,
				BirthDate:   date,
			}
			actual, err := serv.CreateUser(ctx, cur)
			if err != nil {
				t.Errorf("FAIL: CreateUser\n%v\nActual:\n%v", cur, err)
				return
			}
			if actual.GetEmail() != eng[i] ||
				actual.GetPwdHash() != eng[i] ||
				actual.GetRole() != pbUser.UserRole(role) ||
				actual.GetStatus() != pbUser.UserStatus(status) ||
				actual.GetDispayName() != rus[i] ||
				actual.GetPhoneNumber() != phone ||
				actual.GetBirthDate() != date || actual.GetId() != i {
				t.Errorf("FAIL: CreateUser\n%v\nActual:\n%v", cur, actual)
				return
			} else {
				t.Logf("PASS: CreateUser test #%d", i)
			}
		}
	}

	createUserTest()

	// CreateUser error tests
	createUserTestError := func() {
		tests := []struct {
			description string
			in          pbUser.CreateUserRequest
		}{
			{description: "empty email"},
			{"duplicate email", pbUser.CreateUserRequest{Email: eng[1]}},
			{"duplicate name", pbUser.CreateUserRequest{Email: eng[0], DispayName: rus[1]}},
		}
		for i := range tests {
			if actual, err := serv.CreateUser(ctx, &tests[i].in); err == nil {
				t.Errorf("FAIL: CreateUserError\n%v\nActual:\n%v", &tests[i].in, actual)
				return
			}
			t.Logf("PASS: CreateUserError test #%d", i+1)
		}
	}

	createUserTestError()

	// GetUsers tests
	getUsersTest := func() {
		var (
			usr          *pbUser.UsersRequest
			role, status int32
			date         int64
		)
		for i := int32(0); i < 10; i++ {
			role = i % 2
			status = i % 2
			if i > 0 {
				date = time.Date(int(i), time.Month(i%12), 1, 0, 0, 0, 0, time.UTC).Unix()
			}
			usr = &pbUser.UsersRequest{
				Limit:     10,
				Offset:    0,
				Role:      pbUser.UserRole(role),
				Status:    pbUser.UserStatus(status),
				BirthDate: date,
			}
			actual, err := serv.GetUsers(ctx, usr)
			if err != nil {
				t.Errorf("FAIL: GetUsers\n%v\nActual:\n%v", usr, err)
				return
			}
			for _, u := range actual.GetPayload() {
				if role != 0 && u.GetRole() != usr.Role ||
					status != 0 && u.GetStatus() != usr.Status ||
					i != 0 && u.GetBirthDate() != date {
					t.Errorf("FAIL: GetUsers\n%v\nActual:\n%v", usr, u)
					return
				}
			}
			t.Logf("PASS: GetUsers test #%d", i)
		}
	}

	getUsersTest()

	// GetUser tests
	getUserByEmailTest := func() {
		var ur *pbUser.UserRequest
		for i := int32(1); i < 10; i++ {
			ur = &pbUser.UserRequest{Email: eng[i]}
			actual, err := serv.GetUser(ctx, ur)
			if err != nil {
				t.Errorf("FAIL: GetUser\n%v\nActual:\n%v", ur, err)
				return
			}
			if actual.GetId() != i || actual.GetEmail() != eng[i] ||
				actual.GetDispayName() != rus[i] || actual.GetPwdHash() != eng[i] {
				t.Errorf("FAIL: GetUser\n%v\nActual:\n%v", ur, actual)
				return
			}
			t.Logf("PASS: GetUser test #%d", i)
		}
	}

	getUserByEmailTest()

	// UpdateUser tests
	updateUserTest := func() {
		var (
			uur          *pbUser.UpdateUserRequest
			idx          = 99
			role, status int32
			date         int64
		)
		for i := int32(1); i < 100; i++ {
			phone := randPhone(rng, 10)
			role = i % 2
			status = i % 2
			idx++
			date = time.Date(int(i+1), time.Month(i%12), 1, 0, 0, 0, 0, time.UTC).Unix()
			uur = &pbUser.UpdateUserRequest{
				Id:          i,
				Email:       eng[idx],
				PwdHash:     eng[idx],
				Role:        pbUser.UserRole(role),
				Status:      pbUser.UserStatus(status),
				DispayName:  rus[idx],
				PhoneNumber: phone,
				BirthDate:   date,
			}
			actual, err := serv.UpdateUser(ctx, uur)
			if err != nil {
				t.Errorf("FAIL: UpdateUser\n%v\nActual:\n%v", uur, err)
				return
			}
			if actual.GetEmail() != eng[idx] ||
				actual.GetPwdHash() != eng[idx] ||
				actual.GetRole() != pbUser.UserRole(role) ||
				actual.GetStatus() != pbUser.UserStatus(status) ||
				actual.GetDispayName() != rus[idx] || actual.GetPhoneNumber() != phone ||
				actual.GetBirthDate() != date || actual.GetId() != i {
				t.Errorf("FAIL: UpdateUser\n%v\nActual:\n%v", uur, actual)
				return
			} else {
				t.Logf("PASS: UpdateUser test #%d", i)
			}
		}
	}

	updateUserTest()

	// UpdateUser error tests
	updateUserTestError := func() {
		tests := []struct {
			description string
			in          pbUser.UpdateUserRequest
		}{
			{"empty email", pbUser.UpdateUserRequest{Id: 21}},
			{"duplicate email", pbUser.UpdateUserRequest{Id: 1, Email: eng[101]}},
			{"duplicate name", pbUser.UpdateUserRequest{Id: 2, Email: eng[2], DispayName: rus[102]}},
		}
		for i := range tests {
			if actual, err := serv.UpdateUser(ctx, &tests[i].in); err == nil {
				t.Errorf("FAIL: UpdateUserTestError\n%s\n%v\nActual:\n%v", tests[i].description, &tests[i].in, actual)
				return
			}
			t.Logf("PASS: UpdateUserError test #%d", i+1)
		}
	}

	updateUserTestError()

	// DeleteUser tests
	deleteUserTest := func() {
		dur := &pbUser.DeleteUserRequest{
			Id: 99,
		}
		_, err := serv.DeleteUser(ctx, dur)
		if err != nil {
			t.Errorf("FAIL: DeleteUser\n%v\nActual:\n%v", dur, err)
			return
		}
		t.Logf("PASS: DeleteUser test #%d", 1)
	}

	deleteUserTest()

	// DeleteUser error tests
	deleteUserTestError := func() {
		dur := &pbUser.DeleteUserRequest{
			Id: 99,
		}
		if actual, err := serv.DeleteUser(ctx, dur); err == nil {
			t.Errorf("FAIL: DeleteUserTestError\n%v\nActual:\n%v", dur, actual)
			return
		}
		t.Logf("PASS: DeleteUserError test #%d", 1)
	}

	deleteUserTestError()
}
