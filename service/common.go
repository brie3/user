package service

import (
	"context"
	"log"
	"runtime/debug"
	"strings"

	"github.com/go-sql-driver/mysql"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

const (
	internalFormat = "internal error"
	errorFormat    = "[%s] error handled: %v"
	recoverFormat  = "[%s] recovered from %v, %s"

	mysqlDuplicateCode uint16 = 1062

	displayName    = "display_name"
	email          = "email"
	smallEmail     = "invalid email"
	duplicateEmail = "duplicate email"
	duplicateName  = "duplicate name"
	key            = "x-request-id"

	createUserQuery         = `insert into users (email,pwd_hash,role,status,display_name,phone_number,birth_date) values (?,?,?,?,?,?,?)`
	createUserErrFormat     = "can't execute user creation query: %v"
	lastCreateUserErrFormat = "can't get last inserted id on user creation: %v"

	getUserIdQuery          = "select * from users where id = ?"
	getUserEmailQuery       = "select * from users where email = ?"
	getUserInvalidFormat    = "invalid given user id and email"
	getUserByIDErrFormat    = "can't execute user get by id query: %v"
	getUserByEmailErrFormat = "can't execute user get by email query: %v"

	getUsersCount          = "select count(*) from users"
	getUsersQuery          = "select * from users"
	orderBy                = " order by display_name, id"
	limit                  = " limit "
	where                  = " where "
	and                    = " and "
	role                   = "role = "
	statusequals           = "status = "
	birth                  = "birth_date = '"
	comma                  = ", "
	quote                  = '\''
	getUsersCountErrFormat = "can't execute get users count query: %v"
	getUsersErrFormat      = "can't execute get users query: %v"
	scanUsersErrFormat     = "can't execute scan row for get users query: %v"

	updateUserQuery     = `update users set email = ?, pwd_hash = ?, role = ?, status = ?, display_name = ?, phone_number = ?, birth_date = ? where id = ?`
	updateUserErrFormat = "can't execute update user query: %v"

	deleteUserQuery          = "delete from users where id = ?"
	deleteUserErrFormat      = "can't execute user deletion query: %v"
	deleteUserNotFoundFormat = "can't execute user deletion for given id: %v; not found"
)

func userCheck(err error) error {
	if me, ok := err.(*mysql.MySQLError); ok && me.Number == mysqlDuplicateCode {
		if strings.Contains(me.Message, email) {
			return status.Error(codes.AlreadyExists, duplicateEmail)
		}
		if strings.Contains(me.Message, displayName) {
			return status.Error(codes.AlreadyExists, duplicateName)
		}
	}
	return nil
}

func wrapRecover(ctx context.Context, r interface{}) error {
	log.Printf(recoverFormat, getRid(ctx), r, debug.Stack())
	return status.Error(codes.Internal, internalFormat)
}

func wrapError(ctx context.Context, err error) error {
	log.Printf(errorFormat, getRid(ctx), err)
	return err
}

func getRid(ctx context.Context) string {
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if arr := md.Get(key); len(arr) > 0 {
			return arr[0]
		}
	}
	return ""
}

// RecoverInterceptor handles recovery from unexpected service behavior.
func RecoverInterceptor(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (resp interface{}, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = wrapRecover(ctx, r)
		}
	}()
	return handler(ctx, req)
}
