package service

import (
	"context"
	pbUser "user/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// UpdateUser updates user.
func (u *UserService) UpdateUser(ctx context.Context, req *pbUser.UpdateUserRequest) (*pbUser.User, error) {
	if len(req.GetEmail()) == 0 {
		return nil, status.Errorf(codes.InvalidArgument, smallEmail)
	}
	if len(req.GetDispayName()) == 0 {
		req.DispayName = req.GetEmail()
	}
	if _, err := u.db.ExecContext(ctx, updateUserQuery, req.GetEmail(), req.GetPwdHash(),
		req.GetRole(), req.GetStatus(), req.GetDispayName(), req.GetPhoneNumber(),
		req.GetBirthDate(), req.GetId()); err != nil {
		if er := userCheck(err); er != nil {
			return nil, wrapError(ctx, er)
		}
		return nil, wrapError(ctx, status.Errorf(codes.Internal, updateUserErrFormat, err))
	}
	return u.GetUser(ctx, &pbUser.UserRequest{Id: req.GetId()})
}
