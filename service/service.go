// Package service implements mysql user db protobuf interactions.
package service

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database"
	"github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
)

type Config struct {
	MaxOpenConns    int
	MaxIdleConns    int
	ConnectRetries  int
	PrometheusAddr  string
	ServiceAddr     string
	DBHost          string
	DBName          string
	DBUser          string
	DBPwd           string
	HealthAddr      string
	ConnMaxLifetime time.Duration
	SleepSeconds    time.Duration
}

// New brings to live new movie service.
func New(c *Config) (out *UserService, err error) {
	var pool *sql.DB
	if pool, err = dbConnect(c); err != nil {
		return
	}
	if err = setupMigrations(pool, c); err != nil {
		return nil, fmt.Errorf("can't migrate: %v; closing db connection: %v", err, pool.Close())
	}
	pool.SetConnMaxLifetime(c.ConnMaxLifetime)
	pool.SetMaxOpenConns(c.MaxOpenConns)
	pool.SetMaxIdleConns(c.MaxIdleConns)
	out = &UserService{db: pool}
	return
}

// UserService stands interactions.
type UserService struct {
	db *sql.DB
}

func (u *UserService) Stop() error {
	return u.db.Close()
}

func SetupVars() (out *Config, err error) {
	var ok bool
	conf := &Config{}
	conf.PrometheusAddr, ok = os.LookupEnv("PROMETHEUS_ADDR")
	if !ok {
		return nil, errors.New("PROMETHEUS_ADDR env is not set")
	}
	conf.ServiceAddr, ok = os.LookupEnv("SERVICE_ADDR")
	if !ok {
		return nil, errors.New("SERVICE_ADDR env is not set")
	}
	conf.DBHost, ok = os.LookupEnv("DATABASE_HOST")
	if !ok {
		return nil, errors.New("DATABASE_HOST env is not set")
	}
	conf.DBName, ok = os.LookupEnv("DATABASE_NAME")
	if !ok {
		return nil, errors.New("DATABASE_NAME env is not set")
	}
	conf.DBUser, ok = os.LookupEnv("DATABASE_USER")
	if !ok {
		return nil, errors.New("DATABASE_USER env is not set")
	}
	conf.DBPwd, ok = os.LookupEnv("DATABASE_PASSWORD")
	if !ok {
		return nil, errors.New("DATABASE_PASSWORD env is not set")
	}

	conf.HealthAddr, ok = os.LookupEnv("HEALTH_ADDR")
	if !ok {
		return nil, errors.New("HEALTH_ADDR env is not set")
	}

	var (
		connectRetries, sleepSeconds string
		n                            int
	)

	connectRetries, ok = os.LookupEnv("CONNECT_RETRIES")
	if n, err = strconv.Atoi(connectRetries); ok && err == nil {
		conf.ConnectRetries = n
	} else {
		conf.ConnectRetries = 12
	}

	sleepSeconds, ok = os.LookupEnv("SLEEP_SECONDS")
	if n, err = strconv.Atoi(sleepSeconds); ok && err == nil {
		conf.SleepSeconds = time.Duration(n)
	} else {
		conf.SleepSeconds = 15
	}

	if conf.ConnMaxLifetime == 0 {
		conf.ConnMaxLifetime = time.Minute * 3
	}
	if conf.MaxOpenConns == 0 {
		conf.MaxOpenConns = 3
	}
	if conf.MaxIdleConns == 0 {
		conf.MaxIdleConns = 3
	}
	return conf, nil
}

func dbConnect(conf *Config) (out *sql.DB, err error) {
	for i := 1; i < conf.ConnectRetries; i++ {
		if out, err = sql.Open("mysql",
			fmt.Sprintf("%s:%s@tcp(%s)/%s?parseTime=true&charset=utf8&interpolateParams=true&multiStatements=true&collation=utf8_general_ci",
				conf.DBUser, conf.DBPwd, conf.DBHost, conf.DBName)); err == nil {
			break
		}
		log.Printf("can't connect to user db: %v", err)
		time.Sleep(time.Second * conf.SleepSeconds)
	}
	for i := 1; i < conf.ConnectRetries; i++ {
		if err = out.Ping(); err == nil {
			break
		}
		log.Printf("can't ping to user db: %v", err)
		time.Sleep(time.Second * conf.SleepSeconds)
	}
	if err != nil && out != nil {
		return nil, fmt.Errorf("can't connect to user db: %v; closing connection: %v", err, out.Close())
	}
	return
}

func setupMigrations(db *sql.DB, c *Config) (err error) {
	var driver database.Driver
	if driver, err = mysql.WithInstance(db, &mysql.Config{DatabaseName: c.DBName}); err != nil {
		return
	}

	var migration *migrate.Migrate
	if migration, err = migrate.NewWithDatabaseInstance("file:///migrations", c.DBName, driver); err != nil {
		return
	}

	if err = migration.Up(); err != nil && err != migrate.ErrNoChange {
		return
	}
	return nil
}
