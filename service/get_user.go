package service

import (
	"context"
	pbUser "user/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetUser gets user.
func (u *UserService) GetUser(ctx context.Context, req *pbUser.UserRequest) (*pbUser.User, error) {
	if req.GetId() != 0 {
		return u.getByID(ctx, req)
	} else if len(req.GetEmail()) != 0 {
		return u.getEmail(ctx, req)
	}
	return nil, wrapError(ctx, status.Error(codes.InvalidArgument, getUserInvalidFormat))
}

func (u *UserService) getByID(ctx context.Context, req *pbUser.UserRequest) (*pbUser.User, error) {
	uu := &pbUser.User{}
	if err := u.db.QueryRowContext(ctx, getUserIdQuery, req.GetId()).Scan(
		&uu.Id, &uu.Email, &uu.PwdHash, &uu.Role, &uu.Status, &uu.DispayName,
		&uu.PhoneNumber, &uu.BirthDate, &uu.CreatedAt, &uu.UpdatedAt); err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, getUserByIDErrFormat, err))
	}
	return uu, nil
}

func (u *UserService) getEmail(ctx context.Context, req *pbUser.UserRequest) (*pbUser.User, error) {
	uu := &pbUser.User{}
	if err := u.db.QueryRowContext(ctx, getUserEmailQuery, req.GetEmail()).Scan(
		&uu.Id, &uu.Email, &uu.PwdHash, &uu.Role, &uu.Status, &uu.DispayName,
		&uu.PhoneNumber, &uu.BirthDate, &uu.CreatedAt, &uu.UpdatedAt); err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, getUserByEmailErrFormat, err))
	}
	return uu, nil
}
