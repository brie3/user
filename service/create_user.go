package service

import (
	"context"
	"database/sql"
	pbUser "user/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// CreateUser creates new user.
func (u *UserService) CreateUser(ctx context.Context, req *pbUser.CreateUserRequest) (*pbUser.User, error) {
	if len(req.GetEmail()) == 0 {
		return nil, wrapError(ctx, status.Errorf(codes.InvalidArgument, smallEmail))
	}
	var (
		err error
		res sql.Result
	)
	if len(req.GetDispayName()) == 0 {
		req.DispayName = req.GetEmail()
	}
	if res, err = u.db.ExecContext(ctx, createUserQuery, req.GetEmail(), req.GetPwdHash(),
		req.GetRole(), req.GetStatus(), req.GetDispayName(), req.GetPhoneNumber(), req.GetBirthDate()); err != nil {
		if er := userCheck(err); er != nil {
			return nil, wrapError(ctx, er)
		}
		return nil, wrapError(ctx, status.Errorf(codes.Internal, createUserErrFormat, err))
	}
	id, err := res.LastInsertId()
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, lastCreateUserErrFormat, err))
	}
	return u.GetUser(ctx, &pbUser.UserRequest{Id: int32(id)})
}
