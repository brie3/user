package service

import (
	"context"
	pbUser "user/proto"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// DeleteUser deletes user.
func (u *UserService) DeleteUser(ctx context.Context, req *pbUser.DeleteUserRequest) (*empty.Empty, error) {
	res, err := u.db.ExecContext(ctx, deleteUserQuery, req.GetId())
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, deleteUserErrFormat, err))
	}
	if count, err := res.RowsAffected(); count == 0 {
		return nil, wrapError(ctx, status.Errorf(codes.InvalidArgument, deleteUserNotFoundFormat, req.GetId()))
	} else if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, deleteUserErrFormat, err))
	}
	return &empty.Empty{}, nil
}
