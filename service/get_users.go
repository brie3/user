package service

import (
	"bytes"
	"context"
	"strconv"
	pbUser "user/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetUsers gets users.
func (u *UserService) GetUsers(ctx context.Context, req *pbUser.UsersRequest) (*pbUser.Users, error) {
	resp := &pbUser.Users{Payload: make([]*pbUser.User, 0, req.Limit)}
	query := &bytes.Buffer{}

	writeCount(query, req)
	if err := u.db.QueryRowContext(ctx, query.String()).Scan(&resp.Total); err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, getUsersCountErrFormat, err))
	}
	query.Reset()

	writeQuery(query, req)
	rows, err := u.db.QueryContext(ctx, query.String())
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, getUsersErrFormat, err))
	}
	for rows.Next() {
		uu := &pbUser.User{}
		if err = rows.Scan(
			&uu.Id, &uu.Email, &uu.PwdHash, &uu.Role,
			&uu.Status, &uu.DispayName, &uu.PhoneNumber, &uu.BirthDate,
			&uu.CreatedAt, &uu.UpdatedAt); err != nil {
			return nil, wrapError(ctx, status.Errorf(codes.Internal, scanUsersErrFormat, err))
		}
		resp.Payload = append(resp.Payload, uu)
	}
	return resp, nil
}

func writeCount(w *bytes.Buffer, req *pbUser.UsersRequest) {
	w.WriteString(getUsersCount)
	writeStatements(w, req, true)
}

func writeQuery(w *bytes.Buffer, req *pbUser.UsersRequest) {
	w.WriteString(getUsersQuery)
	writeStatements(w, req, true)
	w.WriteString(orderBy)
	writeLimit(w, req)
}

func writeStatements(w *bytes.Buffer, req *pbUser.UsersRequest, isFirst bool) {
	if req.GetRole() == pbUser.UserRole_UserRole_NOTSET &&
		req.GetStatus() == pbUser.UserStatus_UserStatus_NOTSET &&
		req.GetBirthDate() == 0 {
		return
	}

	if isFirst {
		w.WriteString(where)
	}

	if req.GetRole() != pbUser.UserRole_UserRole_NOTSET {
		if !isFirst {
			w.WriteString(and)
		}
		w.WriteString(role)
		w.WriteString(strconv.Itoa(int(req.GetRole())))
		isFirst = false
	}

	if req.GetStatus() != pbUser.UserStatus_UserStatus_NOTSET {
		if !isFirst {
			w.WriteString(and)
		}
		w.WriteString(statusequals)
		w.WriteString(strconv.Itoa(int(req.GetStatus())))
		isFirst = false
	}

	if req.GetBirthDate() != 0 {
		if !isFirst {
			w.WriteString(and)
		}
		w.WriteString(birth)
		w.WriteString(strconv.Itoa(int(req.GetBirthDate())))
		w.WriteByte(quote)
	}
}

func writeLimit(w *bytes.Buffer, req *pbUser.UsersRequest) {
	w.WriteString(limit)
	w.WriteString(strconv.Itoa(int(req.GetOffset())))
	w.WriteString(comma)
	w.WriteString(strconv.Itoa(int(req.GetLimit())))
}
